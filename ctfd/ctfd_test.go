package ctfd

import (
	"testing"
)

func TestValidName(t *testing.T) {
	var err error

	err = validateName("compfest_11")

	if err != nil {
		t.Log(`"compfest_11" should be valid!"`)
		t.Fail()
	}

	err = validateName("9front")

	if err != nil {
		t.Log(`"9front" numeric containing name should be valid!"`)
		t.Fail()
	}
}

func TestInvalidName(t *testing.T) {
	var err error

	err = validateName("Compfest 11")

	if err == nil {
		t.Log(`"Compfest 11" should be invalid!"`)
		t.Fail()
	}

	// wait for https://github.com/moby/moby/commit/0243936d92ea57f48c6a32beac797ee3b388ba28#r33015332
	// err = validateName("ctfd.io")

	// if err == nil {
	// 	t.Log(`"ctfd.io" should be invalid!"`)
	// 	t.Fail()
	// }
}

func TestValidHostname(t *testing.T) {
	var err error

	err = validateHostname("compfest-11")

	if err != nil {
		t.Log(`"compfest-11" should be valid!"`)
		t.Fail()
	}

	err = validateHostname("9front")

	if err != nil {
		t.Log(`"9front" numeric containing name should be valid!"`)
		t.Fail()
	}
}

func TestInvalidHostname(t *testing.T) {
	var err error

	err = validateHostname("Compfest 11")

	if err == nil {
		t.Log(`"Compfest 11" should be invalid!"`)
		t.Fail()
	}

	// wait for https://github.com/moby/moby/commit/0243936d92ea57f48c6a32beac797ee3b388ba28#r33015332
	// err = validateHostname("ctfd.io")

	// if err == nil {
	// 	t.Log(`"ctfd.io-" should be invalid!"`)
	// 	t.Fail()
	// }
}

// func TestValidNew(t *testing.T) {
// 	var err error
//
// 	_, err = New("compfest_11")
//
// 	if err != nil {
// 		t.Log(`"compfest_11" should be valid!"`)
// 		t.Fail()
// 	}
//
// 	_, err = New("9front")
//
// 	if err != nil {
// 		t.Log(`"9front" numeric containing name should be valid!"`)
// 		t.Fail()
// 	}
// }
//
// func TestInvalidNew(t *testing.T) {
// 	var err error
//
// 	_, err = New("Compfest 11")
//
// 	if err == nil {
// 		t.Log(`"Compfest 11" should be invalid!"`)
// 		t.Fail()
// 	}
//
// 	_, err = New("ctfd.io")
//
// 	if err == nil {
// 		t.Log(`"ctfd.io" should be invalid!"`)
// 		t.Fail()
// 	}
// }
