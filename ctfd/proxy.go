package ctfd

import (
	"context"
	"errors"
	"log"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
)

const (
	ctfddHostname = "ctf.cs.ui.ac.id"

	proxyNetwork = "ctfdd_backend"
	proxyService = "ctfdd_proxy"

	FORCE_HTTPS = true
)

var proxyNetworkId = getProxyNetworkId()

func SetupProxy() error {
	// Check if backend network already exists
	if err := isNetworkConfigured(); err != nil {
		log.Print(err)
		log.Print("SetupProxy(): Trying to configure...")

		if err := setupNetwork(); err != nil {
			return err
		}
	}

	// Check traefik is configured or not
	if err := isProxyConfigured(); err != nil {
		log.Print(err)
		log.Print("SetupProxy(): Trying to configure...")

		if err := setupProxy(); err != nil {
			return err
		}
	}
	return nil
}

func isNetworkConfigured() error {
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}

	ctx := context.Background()

	f := filters.NewArgs()
	f.Add("label", labelProject)
	f.Add("label", labelNetwork+"="+proxyNetwork)

	opts := types.NetworkListOptions{Filters: f}

	networks, err := cli.NetworkList(ctx, opts)
	if err != nil {
		return err
	}

	switch len(networks) {
	case 0:
		return errors.New("isNetworkConfigured(): Network is not configured")
	case 1:
		break
	default:
		return errors.New("isNetworkConfigured(): Network is misconfigured")
	}

	backend_network := networks[0]
	log.Printf("Configured backend network: %s", backend_network.Name)

	return nil
}

func isProxyConfigured() error {
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}

	ctx := context.Background()

	f := filters.NewArgs()
	f.Add("label", labelProject)
	f.Add("label", labelService+"="+proxyService)

	opts := types.ContainerListOptions{All: true, Filters: f}

	containerList, err := cli.ContainerList(ctx, opts)
	if err != nil {
		return err
	}

	switch len(containerList) {
	case 0:
		return errors.New("isProxyConfigured(): Proxy is not configured")
	case 1:
		break
	default:
		return errors.New("isProxyConfigured(): Proxy is misconfigured")
	}

	proxy, err := cli.ContainerInspect(ctx, containerList[0].ID)
	if err != nil {
		return err
	}

	if !proxy.State.Running {
		return errors.New("isProxyConfigured(): Proxy is not running")
	}

	containerName := strings.TrimPrefix(proxy.Name, "/")
	log.Printf("Configured proxy: %s", containerName)

	return nil
}

func setupNetwork() error {
	return errors.New("setupNetwork(): Not implemented yet")
}

func setupProxy() error {
	return errors.New("setupProxy(): Not implemented yet")
}

func configProxy(config *container.Config, hostname string) {
	key := "traefik.enable"
	value := "true"
	config.Labels[key] = value

	key = "traefik.http.routers." + hostname + ".rule"
	value = "Host(`" + hostname + "." + ctfddHostname + "`)"
	config.Labels[key] = value

	if FORCE_HTTPS {
		key = "traefik.http.routers." + hostname + ".tls"
		config.Labels[key] = ""
	}

	key = "traefik.http.services." + hostname + ".loadbalancer.server.port"
	value = "8000"
	config.Labels[key] = value
}

func configProxyHost(config *container.HostConfig) {
	config.NetworkMode = proxyNetwork
}

func configProxyNetwork(netConfig *network.NetworkingConfig, hostname string) {
	netConfig.EndpointsConfig[proxyNetwork] = &network.EndpointSettings{
		Aliases: []string{hostname},
	}
}

func getProxyNetworkId() string {
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	f := filters.NewArgs()
	f.Add("label", labelProject)
	f.Add("label", labelNetwork+"="+proxyNetwork)

	opts := types.NetworkListOptions{Filters: f}

	networks, err := cli.NetworkList(ctx, opts)
	if err != nil {
		panic(err)
	}

	if len(networks) != 1 {
		log.Print(networks)
		panic(errors.New("getProxyNetworkId(): Misconfigured backend network"))
	}

	return networks[0].ID
}
