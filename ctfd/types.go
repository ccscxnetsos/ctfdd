package ctfd

type CTFd struct {
	Spec   CTFdSpec   `json:"spec"`
	Status CTFdStatus `json:"status,omitempty"`
}

type CTFdStatus struct {
	State   string `json:"state,omitempty"`
	Message string `json:"message,omitempty"`
}

type CTFdSpec struct {
	Name     string `json:"name"`
	Hostname string `json:"hostname"`
}

type Filter struct {
	All bool // Running and stopped instances
}
