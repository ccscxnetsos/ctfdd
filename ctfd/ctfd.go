/*
Package ctfd provides CTFd type and ways to interact with them.
*/

package ctfd

import (
	"context"
	"errors"
	"regexp"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/volume"
	"github.com/docker/docker/client"
)

const (
	// Regex string to match hostname according to RFC-1123
	hostnameRFC1123RegexString = `^[a-zA-Z0-9][a-zA-Z0-9\-\.]+[a-z-Az0-9]$`

	// Regex string to match docker container naming
	containerRegexString = `^[a-zA-Z0-9][a-zA-Z0-9_.-]+$`

	// Labels attached to docker resources
	labelNetwork = "id.ac.ui.cs.ctf.network"
	labelProject = "id.ac.ui.cs.ctf.project"
	labelService = "id.ac.ui.cs.ctf.service"
	labelVolume  = "id.ac.ui.cs.ctf.volume"

	// Images name
	ctfdImage    = "docker.io/ctfd/ctfd"
	mariadbImage = "docker.io/library/mariadb:10.4"
	redisImage   = "docker.io/library/redis:4"
)

var (
	hostnameRFC1123Regex = regexp.MustCompile(hostnameRFC1123RegexString)
	containerRegex       = regexp.MustCompile(containerRegexString)
)

// New returns a pointer to an instantiated CTFd object. If the given name is
// not a URL safe name, this method will return non-nil err return value.
func New(name string) (*CTFd, error) {
	err := validateName(name)
	if err != nil {
		return nil, err
	}

	ctfd := CTFd{
		Spec: CTFdSpec{Name: name, Hostname: name},
	}

	cli, err := client.NewEnvClient()
	if err != nil {
		return nil, err
	}

	ctx := context.Background()

	var (
		prjName = name

		// Define internal
		internalNet = name + "_internal"

		// Define services
		ctfdService  = name + "_ctfd"
		dbService    = name + "_db"
		cacheService = name + "_cache"

		// Define container params
		config     container.Config
		hostConfig container.HostConfig
		netConfig  network.NetworkingConfig

		alwaysRestart = container.RestartPolicy{Name: "always"}
	)

	// Pull newer images

	imgs := []string{ctfdImage, mariadbImage, redisImage}

	for _, img := range imgs {
		reader, err := cli.ImagePull(ctx, img, types.ImagePullOptions{})
		if err != nil {
			return nil, err
		}

		defer reader.Close()
	}

	// Create internal network

	_, err = cli.NetworkCreate(ctx, internalNet,
		types.NetworkCreate{
			CheckDuplicate: true,
			Labels: map[string]string{
				labelNetwork: internalNet,
				labelProject: prjName,
			},
		})

	if err != nil {
		return nil, err
	}

	// Create volumes

	for _, vol := range []string{ctfdService, dbService, cacheService} {
		_, err := cli.VolumeCreate(ctx, volume.VolumesCreateBody{
			Name: vol,
			Labels: map[string]string{
				labelProject: prjName,
				labelVolume:  vol,
			},
		})

		if err != nil {
			return nil, err
		}
	}

	// Create cache

	config = container.Config{
		Image:        redisImage,
		AttachStdout: true,
		AttachStderr: true,
		Labels: map[string]string{
			labelProject: prjName,
			labelService: "cache",
		},
		Volumes: map[string]struct{}{
			"/data": {},
		},
	}

	hostConfig = container.HostConfig{
		Binds: []string{
			cacheService + ":/data",
		},
		NetworkMode:   container.NetworkMode(internalNet),
		RestartPolicy: alwaysRestart,
	}

	netConfig = network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			internalNet: &network.EndpointSettings{
				Aliases: []string{"cache"},
			},
		},
	}

	_, err = cli.ContainerCreate(ctx, &config, &hostConfig, &netConfig, cacheService)

	if err != nil {
		return nil, err
	}

	// Create db

	config = container.Config{
		Image:        mariadbImage,
		AttachStdout: true,
		AttachStderr: true,
		Labels: map[string]string{
			labelProject: prjName,
			labelService: "db",
		},
		Volumes: map[string]struct{}{
			"/var/lib/mysql": {},
		},
		Env: []string{
			"MYSQL_ROOT_PASSWORD=ctfd",
			"MYSQL_USER=ctfd",
			"MYSQL_PASSWORD=ctfd",
		},
		Cmd: []string{
			"mysqld", "--character-set-server=utf8mb4",
			"--collation-server=utf8mb4_unicode_ci", "--wait_timeout=28800",
			"--log-warnings=0",
		},
	}

	hostConfig = container.HostConfig{
		Binds: []string{
			dbService + ":/var/lib/mysql",
		},
		NetworkMode:   container.NetworkMode(internalNet),
		RestartPolicy: alwaysRestart,
	}

	netConfig = network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			internalNet: &network.EndpointSettings{
				Aliases: []string{"db"},
			},
		},
	}

	_, err = cli.ContainerCreate(ctx, &config, &hostConfig, &netConfig, dbService)

	if err != nil {
		return nil, err
	}

	// Create ctfd

	config = container.Config{
		Image:        ctfdImage,
		AttachStdout: true,
		AttachStderr: true,
		Env: []string{
			"UPLOAD_FOLDER=/var/uploads",
			"DATABASE_URL=mysql+pymysql://root:ctfd@db/ctfd",
			"REDIS_URL=redis://cache:6379",
			"WORKERS=1",
			"LOG_FOLDER=/var/log/CTFd",
			"ACCESS_LOG=-",
			"ERROR_LOG=-",
		},
		Labels: map[string]string{
			labelProject: prjName,
			labelService: "ctfd",
		},
	}

	configProxy(&config, name)

	hostConfig = container.HostConfig{
		Binds: []string{
			ctfdService + ":/var/lib/mysql",
		},
		NetworkMode:   container.NetworkMode(internalNet),
		RestartPolicy: alwaysRestart,
	}

	netConfig = network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			internalNet: &network.EndpointSettings{
				Aliases: []string{"ctfd"},
			},
		},
	}

	_, err = cli.ContainerCreate(ctx, &config, &hostConfig, &netConfig, ctfdService)

	f := filters.NewArgs()
	f.Add("label", labelProject+"="+prjName)
	f.Add("label", labelService+"=ctfd")

	opts := types.ContainerListOptions{All: true, Filters: f}

	containers, err := cli.ContainerList(ctx, opts)
	if err != nil {
		return nil, err
	} else if len(containers) != 1 {
		return nil, errors.New("")
	}

	cli.NetworkConnect(ctx, proxyNetworkId, containers[0].ID,
		&network.EndpointSettings{Aliases: []string{"ctfdService"}})

	ctfd.Status = CTFdStatus{State: "created"} //TODO Goblog hardcoded wkwk

	if err != nil {
		return nil, err
	}

	return &ctfd, nil
}

// getCTFd return CTFd instance from ctfd container
func getCTFd(container *types.Container) CTFd {
	return CTFd{
		Spec: CTFdSpec{
			Name:     container.Labels[labelProject],
			Hostname: container.Labels[labelProject],
		},
		Status: CTFdStatus{State: container.Status},
	}
}

func GetInstances(filter *Filter) ([]CTFd, error) {
	cli, err := client.NewEnvClient()
	if err != nil {
		return nil, err
	}

	ctx := context.Background()

	f := filters.NewArgs()
	f.Add("label", labelProject)
	f.Add("label", labelService+"=ctfd")

	opts := types.ContainerListOptions{All: filter.All, Filters: f}

	containers, err := cli.ContainerList(ctx, opts)
	if err != nil {
		return nil, err
	}

	ctfds := make([]CTFd, 0)

	for _, container := range containers {
		ctfds = append(ctfds, getCTFd(&container))
	}

	return ctfds, nil
}

func Run(name string) error {
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}

	ctx := context.Background()

	f := filters.NewArgs()
	f.Add("label", labelProject+"="+name)

	opts := types.ContainerListOptions{All: true, Filters: f}

	containers, err := cli.ContainerList(ctx, opts)
	if err != nil {
		return err
	}

	var dbId string = ""

	for _, container := range containers {
		if service, ok := container.Labels[labelService]; ok && service == "db" {
			dbId = container.ID
			break
		}
	}

	if dbId == "" {
		return errors.New("Run(): db service not found")
	}

	err = cli.ContainerStart(ctx, dbId, types.ContainerStartOptions{})
	if err != nil {
		return err
	}

	var cacheId string = ""

	for _, container := range containers {
		if service, ok := container.Labels[labelService]; ok && service == "cache" {
			cacheId = container.ID
			break
		}
	}

	if cacheId == "" {
		return errors.New("Run(): cache service not found")
	}

	err = cli.ContainerStart(ctx, cacheId, types.ContainerStartOptions{})
	if err != nil {
		return err
	}

	var ctfdId string = ""

	for _, container := range containers {
		if service, ok := container.Labels[labelService]; ok && service == "ctfd" {
			ctfdId = container.ID
			break
		}
	}

	if ctfdId == "" {
		return errors.New("Run(): ctfd service not found")
	}

	err = cli.ContainerStart(ctx, ctfdId, types.ContainerStartOptions{})
	if err != nil {
		return err
	}

	return nil
}

func Stop(name string) error {
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}

	ctx := context.Background()

	f := filters.NewArgs()
	f.Add("label", labelProject+"="+name)

	timeout := time.Duration(5000)

	opts := types.ContainerListOptions{All: true, Filters: f}

	containers, err := cli.ContainerList(ctx, opts)
	if err != nil {
		return err
	}

	var dbId string = ""

	for _, container := range containers {
		if service, ok := container.Labels[labelService]; ok && service == "db" {
			dbId = container.ID
			break
		}
	}

	if dbId == "" {
		return errors.New("Stop(): db service not found")
	}

	err = cli.ContainerStop(ctx, dbId, &timeout)
	if err != nil {
		return err
	}

	var cacheId string = ""

	for _, container := range containers {
		if service, ok := container.Labels[labelService]; ok && service == "cache" {
			cacheId = container.ID
			break
		}
	}

	if cacheId == "" {
		return errors.New("Stop(): cache service not found")
	}

	err = cli.ContainerStop(ctx, cacheId, &timeout)
	if err != nil {
		return err
	}

	var ctfdId string = ""

	for _, container := range containers {
		if service, ok := container.Labels[labelService]; ok && service == "ctfd" {
			ctfdId = container.ID
			break
		}
	}

	if ctfdId == "" {
		return errors.New("Stop(): ctfd service not found")
	}

	err = cli.ContainerStop(ctx, ctfdId, &timeout)
	if err != nil {
		return err
	}

	return nil
}

// Validate CTFd hostname
func validateHostname(hostname string) error {
	if hostnameRFC1123Regex.MatchString(hostname) {
		return nil
	} else {
		return errors.New("ctfd: Not a valid CTFd hostname")
	}
}

// Validate CTFd name
func validateName(name string) error {
	if containerRegex.MatchString(name) {
		return nil
	} else {
		return errors.New("ctfd: Not a valid CTFd name")
	}
}
