package main

import (
	"log"
	"net/http"

	"gitlab.com/ccscxnetsos/ctfdd/ctfd"
	"gitlab.com/ccscxnetsos/ctfdd/handler"
)

const defaultListenAddress = "0.0.0.0:18963"

func main() {
	log.Print("========================= CTFdd =========================")
	log.Printf("Server started at %s\n", defaultListenAddress)

	err := ctfd.SetupProxy()
	if err != nil {
		log.Fatal(err)
	}

	serveHTTP()
}

func serveHTTP() {
	server := http.NewServeMux()
	handler.Handle(server)
	http.ListenAndServe(defaultListenAddress, server)
}
