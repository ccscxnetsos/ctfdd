package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/ccscxnetsos/ctfdd/ctfd"
)

func Handle(serveMux *http.ServeMux) {
	serveMux.HandleFunc("/instances/", instances)
	serveMux.HandleFunc("/instances/create/", instancesCreate)
	serveMux.HandleFunc("/instances/run/", instancesRun)
	serveMux.HandleFunc("/instances/stop/", instancesStop)
}

func valuesToFilter(query url.Values) ctfd.Filter {
	if query.Get("all") == "1" {
		return ctfd.Filter{All: true}
	}

	return ctfd.Filter{}
}

func instances(w http.ResponseWriter, r *http.Request) {
	f := valuesToFilter(r.URL.Query())

	ctfds, err := ctfd.GetInstances(&f)
	if err != nil {
		panic(err)
	}

	b, err := json.Marshal(ctfds)

	if err != nil {
		panic(err)
	}

	fmt.Fprint(w, string(b))
}

func instancesCreate(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		m := map[string]string{
			"error": "method not allowed",
		}

		b, _ := json.Marshal(m)
		fmt.Fprint(w, string(b))
		return
	}

	mime := r.Header.Get("Content-Type")

	if !strings.HasSuffix(mime, "application/json") {
		panic(errors.New("error: Content-Type not allowed"))
	}

	decoder := json.NewDecoder(r.Body)

	var decoded ctfd.CTFdSpec

	err := decoder.Decode(&decoded)

	if err != nil {
		panic(errors.New("error: unrecognized json format"))
	}

	log.Print(decoded)

	var c *ctfd.CTFd

	c, err = ctfd.New(decoded.Name)

	if err != nil {
		panic(err)
	}

	b, err := json.Marshal(&c)

	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusAccepted)
	fmt.Fprint(w, string(b))
}

func instancesRun(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		m := map[string]string{
			"error": "method not allowed",
		}

		b, _ := json.Marshal(m)
		fmt.Fprint(w, string(b))
		return
	}

	mime := r.Header.Get("Content-Type")

	if !strings.HasSuffix(mime, "application/json") {
		panic(errors.New("error: Content-Type not allowed"))
	}

	decoder := json.NewDecoder(r.Body)

	var decoded map[string]string

	err := decoder.Decode(&decoded)

	if err != nil {
		panic(errors.New("error: unrecognized json format"))
	}

	name, ok := decoded["name"]

	if !ok {
		panic(errors.New(`error: missing "name" field`))
	}

	err = ctfd.Run(name)

	if err != nil {
		panic(err)
	}
}

func instancesStop(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		m := map[string]string{
			"error": "method not allowed",
		}

		b, _ := json.Marshal(m)
		fmt.Fprint(w, string(b))
		return
	}

	mime := r.Header.Get("Content-Type")

	if !strings.HasSuffix(mime, "application/json") {
		panic(errors.New("error: Content-Type not allowed"))
	}

	decoder := json.NewDecoder(r.Body)

	var decoded map[string]string

	err := decoder.Decode(&decoded)

	if err != nil {
		panic(errors.New("error: unrecognized json format"))
	}

	name, ok := decoded["name"]

	if !ok {
		panic(errors.New(`error: missing "name" field`))
	}

	err = ctfd.Stop(name)

	if err != nil {
		panic(err)
	}
}
