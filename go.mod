module gitlab.com/ccscxnetsos/ctfdd

require (
	github.com/Microsoft/go-winio v0.4.12 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0
	github.com/docker/go-units v0.3.3 // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190328230028-74de082e2cca // indirect
	golang.org/x/sys v0.0.0-20190402142545-baf5eb976a8c // indirect
)

go 1.13
